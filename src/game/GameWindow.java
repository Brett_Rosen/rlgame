package game;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JFrame;
import asciiPanel.AsciiPanel;
import screens.Screen;
import screens.StartScreen;

public class GameWindow extends JFrame implements KeyListener {
	private static final long serialVersionUID = -599654434852754547L;
	
	private AsciiPanel terminal;
	private Screen screen;
	
	public GameWindow() {
		// Construct a visible frame and add key listener
		super(); 
		addKeyListener(this);
		
		// Simulate a terminal display
		terminal = new AsciiPanel(80, 21);
		
		screen = new StartScreen();
	
		// Add terminal display to the frame
		add(terminal);
		pack();
		repaint();
	}
	
	// Clear the terminal and display the output appropriate to the given screen
	public void repaint() {
		screen.displayOutput(terminal);
		super.repaint();
	}

	// Whenever a key is pressed, call the screens respondToUserInput method
	@Override
	public void keyPressed(KeyEvent e) {
		screen = screen.respondToUserInput(e);
		repaint();
	}
	
	@Override
	public void keyTyped(KeyEvent e) { }

	@Override
	public void keyReleased(KeyEvent e) { }

}
