package game;
import javax.swing.JFrame;

public class Game  {
	
	public static void main(String[] args) {
		// Create a new instance of extended JFrame
		GameWindow gameWindow = new GameWindow();
		
		// Set close operation and visible
		gameWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		gameWindow.setVisible(true);
	}
	
}
