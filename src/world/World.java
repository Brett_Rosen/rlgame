package world;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

import creature.Creature;

public class World {

	private Tile[][] tiles;
	private int width;
	private int height;
	
	private List<Creature> creatures;
	
	public World(Tile[][] tiles) {
		this.tiles = tiles;
		this.width = tiles.length;
		this.height = tiles[0].length;
		
		creatures = new ArrayList<Creature>();
	}
	
	public void addAtEmptyLocation(Creature creature) {
		int x;
		int y;
		
		// Keep looking for an x,y that is ground and there's no creature on it
		do {
			x = (int) (Math.random() * width);
			y = (int) (Math.random() * height);
		} while(!tile(x,y).isGround() || creature(x,y) != null); 
		
		creature.setX(x);
		creature.setY(y);
		creatures.add(creature);
	}
	
	public void dig(int x, int y) {
		if (tiles[x][y].isDiggable())
			tiles[x][y] = Tile.FLOOR;
	}
	
	public void update() {
		List<Creature> toUpdate = new ArrayList<Creature>(creatures);
	    for (Creature creature : toUpdate){
	        creature.update();
	    }
	}
	
	// Get a creature at a given location
	public Creature creature(int x, int y) {
		for (Creature c: creatures) {
			if (c.getX() == x && c.getY() == y) {
				return c;
			}
		}
		return null;
	}
	
	public void remove(Creature other) {
		creatures.remove(other);
	}
	
	public Tile tile(int x, int y) {
		if (x < 0 || x >= width || y < 0 || y >= height)
			return Tile.BOUNDS;
		else
			return tiles[x][y];
	}
	
	public char glyph(int x, int y) {
		return tiles[x][y].getGlyph();
	}
	
	public Color color(int x, int y) {
		return tiles[x][y].getColor();
	}
	
	public int getWidth() {
		return width;
	}
	public int getHeight() {
		return height;
	}
	
}
