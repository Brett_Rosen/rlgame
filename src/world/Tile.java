package world;

import java.awt.Color;
import asciiPanel.AsciiPanel;

public enum Tile {

	FLOOR((char) 250, AsciiPanel.white),
	WALL((char) 177, AsciiPanel.white),
	BOUNDS('x', AsciiPanel.brightBlack);
	
	private char glyph;
	private Color color;
	
	Tile(char glyph, Color color) { 
		this.glyph = glyph;
		this.color = color;
	}

	public boolean isDiggable() {
		return this == Tile.WALL;
	}
	
	public boolean isGround() {
		return this != WALL && this != BOUNDS;
	}
	
	public char getGlyph() {
		return glyph;
	}

	public Color getColor() {
		return color;
	}
	
}
