package world;

public class WorldBuilder {

	private int width;
	private int height;
	private Tile[][] tiles;
	
	public WorldBuilder(int width, int height) {
		this.width = width;
		this.height = height;
		this.tiles = new Tile[width][height];
	}
	
	public World build() {
		return new World(tiles);
	}
	
	public WorldBuilder makeCaves(int iterations) {
		return randomizeTiles().smooth(iterations);
	}
	
	// Randomizes the tiles in tiles[x][y]
	private WorldBuilder randomizeTiles() {
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				tiles[x][y] = Math.random() < 0.5 ? Tile.FLOOR : Tile.WALL;
			}
		}
		return this;
	}
	
	// Smoothes out tiles[x][y] using cellular automata 
	private WorldBuilder smooth(int times) {
		Tile[][] tempTiles = new Tile[width][height];
		
		// For i number of times
		for (int i = 0; i < times; i++) {
			// For each tiles
			for (int x = 0; x < width; x++) {
				for (int y = 0; y < height; y++) {
					int floors = 0;
					int walls  = 0;
					
					// For the 3x3 grid around that tile, check how many of the 9 tiles are floors or walls
					for (int xx = -1; xx < 2; xx++) {
						for (int yy = -1; yy < 2; yy++) {
							// If there is a location thats out of bounds, go to the next
							if (x + xx < 0 || x + xx >= width || y + yy < 0 || y + yy >= height)
								continue;
							
							if (tiles[x + xx][y + yy] == Tile.FLOOR)
								floors++;
							if (tiles[x + xx][y + yy] == Tile.WALL)
								walls++;
						}
					}
					// For each tile, if there was more floors surrounding it, make it a floor, otherwise make it a wall
					tempTiles[x][y] = floors >= walls ? Tile.FLOOR : Tile.WALL;
				}
			}
			// Update tiles to be smoothed again 
			tiles = tempTiles;
		}
		return this;
	}
	
}
