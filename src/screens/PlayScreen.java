package screens;

import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;
import asciiPanel.AsciiPanel;
import creature.Creature;
import creature.CreatureFactory;
import world.World;
import world.WorldBuilder;

public class PlayScreen implements Screen {
	
	private World world;

	private Creature player;

	private int screenWidth;
	private int screenHeight;
	
	private List<String> messages;
	
	public PlayScreen() {
		this.screenWidth = 80;
		this.screenHeight = 21;
		messages = new ArrayList<String>();
		createWorld();
		
		CreatureFactory creatureFactory = new CreatureFactory(world);
		createCreatures(creatureFactory);
	}

	@Override
	public void displayOutput(AsciiPanel terminal) {
		int left = getScrollX();
		int top = getScrollY();
		displayTiles(terminal, left, top);
		
		terminal.write(player.getGlyph(), player.getX() - left, player.getY() - top, player.getColor());
		String stats = String.format("%3d/%3d hp", player.getHp(), player.getMaxHp());
		terminal.write(stats, 1, 20);
		displayMessages(terminal, messages);
	}

	@Override
	public Screen respondToUserInput(KeyEvent key) {
		switch(key.getKeyCode()) {
			case KeyEvent.VK_ESCAPE:
				return new LoseScreen();
				
			case KeyEvent.VK_ENTER:
				return new WinScreen();
				
			case KeyEvent.VK_LEFT:
	        case KeyEvent.VK_H: player.moveBy(-1, 0); break;
	        
	        case KeyEvent.VK_RIGHT:
	        case KeyEvent.VK_L: player.moveBy( 1, 0); break;
	        
	        case KeyEvent.VK_UP:
	        case KeyEvent.VK_K: player.moveBy( 0,-1); break;
	        
	        case KeyEvent.VK_DOWN:
	        case KeyEvent.VK_J: player.moveBy( 0, 1); break;
	        
	        case KeyEvent.VK_Y: player.moveBy(-1,-1); break;
	        case KeyEvent.VK_U: player.moveBy( 1,-1); break;
	        case KeyEvent.VK_B: player.moveBy(-1, 1); break;
	        case KeyEvent.VK_N: player.moveBy( 1, 1); break;
		}
		
		world.update();
		
		return this;
	}
	
	private void createWorld() {
		int width = 90, height = 31;
		int numberOfSmoothes = 8;
		
		world = new WorldBuilder(width, height)
				.makeCaves(numberOfSmoothes)
				.build();
	}
	
	private void createCreatures(CreatureFactory creatureFactory) {
		player = creatureFactory.newPlayer(messages);
		
		for (int i = 0; i < 8; i++) {
			creatureFactory.newFungus();
		}
	}
	
	private void displayMessages(AsciiPanel terminal, List<String> messages) {
		int top = screenHeight - messages.size();
		for (int i = 0; i < messages.size(); i++) {
			terminal.writeCenter(messages.get(i), top + i);
		}
		messages.clear();
	}
	
	public int getScrollX() {
		return Math.max(0, Math.min(player.getX() - screenWidth / 2, world.getWidth() - screenWidth));
	}
	
	public int getScrollY() {
		return Math.max(0, Math.min(player.getY() - screenHeight / 2, world.getHeight() - screenHeight));
	}
	
	private void displayTiles(AsciiPanel terminal, int left, int top) {
		for (int x = 0; x < screenWidth; x++) {
			for (int y=  0; y < screenHeight; y++) {
				int wx = x + left;
				int wy = y + top;
				
				Creature creature = world.creature(wx,wy);
				if (creature != null) 
	                terminal.write(creature.getGlyph(), creature.getX() - left, creature.getY() - top, creature.getColor());
				else
					terminal.write(world.glyph(wx, wy), x, y, world.color(wx, wy));
			}
		}
	}

}







