package screens;

import java.awt.event.KeyEvent;
import asciiPanel.AsciiPanel;

public class StartScreen implements Screen {

	@Override
	public void displayOutput(AsciiPanel terminal) {
		terminal.writeCenter("-- Main Menu-- ", 1);
		
		terminal.writeCenter("1) New Game", (terminal.getHeightInCharacters() / 2) - 2);
		terminal.write("2) Load Game", (terminal.getWidthInCharacters() / 2) - 6, terminal.getHeightInCharacters() / 2);
	}

	@Override
	public Screen respondToUserInput(KeyEvent key) {
		return key.getKeyCode() == KeyEvent.VK_1 ? new PlayScreen() : this;
	}

}
