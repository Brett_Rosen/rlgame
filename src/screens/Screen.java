package screens;

import java.awt.event.KeyEvent;
import asciiPanel.AsciiPanel;

// Screen interface, each screen must implement the following methods
public interface Screen {
	
	// Display output on the AsciiPanel provided
	public void displayOutput(AsciiPanel terminal);
	
	// Takes a key input and returns a screen
	public Screen respondToUserInput(KeyEvent key);
	
}
