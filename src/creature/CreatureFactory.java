package creature;

import java.util.List;

import asciiPanel.AsciiPanel;
import world.World;

public class CreatureFactory {

	private World world;
	
	public CreatureFactory(World world) {
		this.world = world;
	}
	
	public Creature newPlayer(List<String> messages) {
		Creature player = new Creature(world, '@', AsciiPanel.brightWhite, 100, 20, 5);
		world.addAtEmptyLocation(player);
		new PlayerAI(player, messages);
		return player;
	}
	
	public Creature newFungus() {
		Creature fungus = new Creature(world, 'f', AsciiPanel.green, 10, 0, 10);
		world.addAtEmptyLocation(fungus);
		new FungusAI(fungus, this);
		return fungus;
	}
	
}
