package creature;

import java.awt.Color;

import world.Tile;
import world.World;

public class Creature {

	private World world;
	
	private int x;
	private int y;
	
	private char glyph;
	private Color color;
	
	private CreatureAI ai;
	
	private int maxHp;
    private int hp;
    private int attackValue;
    private int defenseValue;
	
	public Creature(World world, char glyph, Color color, int maxHp, int attack, int defense) {
		this.world = world;
		this.glyph = glyph;
		this.color = color;
		this.maxHp = maxHp;
		this.hp = maxHp;
		this.attackValue = attack;
		this.defenseValue = defense;
	}
	
	public void notify(String message, Object ... params) {
		ai.onNotify(String.format(message, params));
	}
	
	public void moveBy(int mx, int my) {
		Creature other = world.creature(x + mx, y + my);
		
		if (other == null) 
			ai.onEnter(x + mx, y + my, world.tile(x + mx, y + my));
		else
			attack(other);
		
	}
	
	public boolean canEnter(int x, int y) {
		if (world.tile(x, y) != Tile.WALL && world.creature(x, y) == null)
			return true;
		else
			return false;
	}
	
	public void attack(Creature other) {
		int amount = Math.max(0, getAttackValue() - other.getDefenseValue());
		amount = (int) (Math.random() * amount) + 1;
		
		other.modifyHp(-amount);
		
		doAction("attack the '%s' for %d damage", other.glyph, amount);		
		other.notify("The '%s' attacks you for %d damage.", glyph, amount);
	}
	
	public void modifyHp(int amount) {
		hp += amount;
		
		if (hp < 1) {
			doAction("die");
			world.remove(this);
		}
	}
	
	public void doAction(String message, Object ... params) {
		int r = 9;
		for (int ox = -r; ox < r+1; ox++){
	         for (int oy = -r; oy < r+1; oy++){
	             if (ox*ox + oy*oy > r*r)
	                 continue;
	         
	             Creature other = world.creature(x+ox, y+oy);
	         
	             if (other == null)
	                 continue;
	         
	             if (other == this)
	                 other.notify("You " + message + ".", params);
	             else
	                 other.notify(String.format("The '%s' %s.", glyph, makeSecondPerson(message)), params);
	         }
	    }
	}
	
	private String makeSecondPerson(String text){
	    String[] words = text.split(" ");
	    words[0] = words[0] + "s";
	    
	    StringBuilder builder = new StringBuilder();
	    for (String word : words){
	        builder.append(" ");
	        builder.append(word);
	    }
	    
	    return builder.toString().trim();
	}
	
	public void update() {
		ai.onUpdate();
	}
	
	public void dig(int wx, int wy) {
		world.dig(wx, wy);
	}
	
	public char getGlyph() {
		return glyph;
	}
	
	public Color getColor() {
		return color;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}
	
	public void setX(int x) {
		this.x = x;
	}

	public void setY(int y) {
		this.y = y;
	}

	public void setCreatureAI(CreatureAI ai) {
		this.ai = ai;
	}

	public int getMaxHp() {
		return maxHp;
	}

	public int getHp() {
		return hp;
	}

	public int getAttackValue() {
		return attackValue;
	}

	public int getDefenseValue() {
		return defenseValue;
	}
		
}
