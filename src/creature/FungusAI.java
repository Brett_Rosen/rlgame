package creature;

public class FungusAI extends CreatureAI {

	private CreatureFactory creatureFactory;
	private int spreadCount;
	
	public FungusAI(Creature creature, CreatureFactory creatureFactory) {
		super(creature);
		this.creatureFactory = creatureFactory;
	}
	
	public void onUpdate() {
		if (spreadCount < 5 && Math.random() < 0.02)
			spread();
	}

	private void spread() {
		int x = creature.getX() + (int)(Math.random() * 11) - 5;
        int y = creature.getY() + (int)(Math.random() * 11) - 5;

        if (!creature.canEnter(x,y))
        	return;
        
        creature.doAction("spawn a child");
        
        Creature child = creatureFactory.newFungus();
        child.setX(x);
        child.setY(y);
        spreadCount++;
	}
}
