package creature;

import world.Tile;

public class CreatureAI {

	public Creature creature;
	
	public CreatureAI(Creature creature) {
		this.creature = creature;
		this.creature.setCreatureAI(this);
	}
	
	public void onEnter(int x, int y, Tile tile) {
		
	}
	
	public void onUpdate() {
		
	}
	
	public void onNotify(String message) {
		
	}
	
}
